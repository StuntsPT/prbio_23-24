# Assignment 01

### Deadline:

December 19<sup>th</sup>/20<sup>th</sup> 2023


### Format:

Work in pairs. Only one group of 3 will be allowed (**only if required**).


### Delivery:

PDF or HTML via E-mail.


## Introduction

During the course's first weeks you took a *sneak peek* at the world of bioinformatics.
Now you will have to do some **research** to complete your new assignment.


## Objectives

* Create a short presentation (**exactly** 10 min) on a bioinformatics topic of your choice
    * Describe the biological problem;
    * Pinpoint how it is related to bioinformatics;
    * Present some of the used software;
        * Bonus points for links to code repositories
    * Find **at least three** research papers from **peer reviewed**, **indexed** journals on the topic (use the links at the bottom to find them);
        * Report the respective findings;


## Rules

* You can use any software to write your report. But it has to be sent as a finished document (PDF or HTML)
* Do not write "walls of text"
    * Make sure your sentences are **short**
    * Use bullet points as "reminders", develop contents orally
* Rely on figures!
    * Figures and tables **must** be readable
* References are mandatory on the final slide
    * Use reference management software to create it
    * The style you should use is "American Psychological Association (APA) 7th edition"
* The slides can be in English or Portuguese. It's your choice
    * But realistically, you should start writing in English
    * You can also present in either language 
* Make sure you proofread and rehearse your work. You only get one shot
    * Going overtime is extremely damaging to your grade
* After each talk there will be time for questions, so be ready


## Scientific literature search resources:

* [Google scholar](https://scholar.google.com/)
* [b-On](https://www.b-on.pt/)
* [PubMed](https://pubmed.ncbi.nlm.nih.gov/)
