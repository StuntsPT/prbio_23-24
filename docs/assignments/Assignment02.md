# Assignment 02


### Deadline:

January 26<sup>th</sup> 2024


### Format:

Text files


### Delivery:

`git`


## Introduction

You have now learned to use a version control system (VCS) - `git`. Your second task is based on it.
This is a very practical task, and the work-flow you will have to master for it will likely be useful during the rest of your career in bioinformatics.
A decent grip on \*NIX shell is a big help for this task.

The word `stnum` represents your student number for the rest of this assignment.


## Objectives

1. *Hand-made* sequences
    * Rename the file you submitted as your first homework (from **classes[1]**) as `stnum_handmade_sequences.fasta`
    * If you feel you can improve this homework, now is you last chance to correct it
        * In case you make any changes to your original sequences, and want them to be re-graded, please rename the file `stnum_handmade_sequences_corrected.fasta` instead

2. Virtual Machine data
    * Name the files you created for your second homework (from **classes[5]**) as `stnum_fstab.txt` and `stnum_screenfetch.txt` (the fstab and screenfetch files respectively)

3. DNA alignments
    * Download between 20 and 50 sequences of a species (or genera) of your choice, but **make sure all sequences are from the same gene fragment**, from a sequence database of your choice
    * Align your DNA sequences using any alignment software you deem adequate
    * Name your "original" and "aligned" files `stnum_sequences.fasta` and `stnum_aligned.fasta` respectively

4. `gitting` it out
    * Consider the following `git` [repository](https://github.com/StuntsPT/pr_bio_2023-2024_assignments)
    * File a new issue on the repository, where you will state that your username is missing from the `students.csv` file.
    * Add the missing data to `students.csv`
        * "Number" represents your student number
        * "Username" represents your github username
        * Do not make any other changes to the file
        * **Respect the separator!**
    * Commit these changes **with an appropriate message**
    * Add the files mentioned in steps 1, 2 and 3 to the repository in your account
    * Get your changes accepted into the original repository
        * Due to a recent change in github's password use policy, it is required that you clone your repository using the `ssh` link rather than the `https` one.
        * This means you will need to generate an *ssh key*. [Here is a video explaining](https://www.youtube.com/watch?v=5Fcf-8LPvws) how to do it **UNTIL 2:20** (the rest does not apply to your use case).
        * Once you have generated your ssh key pair, you need to add your public key to github. [Here is how](https://docs.github.com/en/authentication/connecting-to-github-with-ssh/adding-a-new-ssh-key-to-your-github-account).

**In order to get a full mark on the assignment, you must make changes to your repository on your local machine/VM, and not using the web interface. The web interface should only be used to fork the repository, open a new issue and make the pull request.**


## Methodology

1. *Hand-made* sequences
    * Methodology is described [here](https://stuntspt.gitlab.io/prbio_23-24/classes/class_02/index.html#/33)

2. Virtual Machine creation
    * Methodology is described [here](https://stuntspt.gitlab.io/prbio_23-24/classes/class_06/index.html#/19)

3. DNA alignments
    * Methodology for this task is detailed [here](https://stuntspt.gitlab.io/prbio_23-24/classes/class_07/index.html#/7) and [here](https://stuntspt.gitlab.io/prbio_23-24/classes/class_07/index.html#/10/1)

4. `gitting` it out
    * Use github's interface to open a new issue in the repository
        * Here is an [issue submission tutorial](https://www.youtube.com/watch?v=TKJ4RdhyB5Y) 
    * Fork [the repository](https://github.com/StuntsPT/pr_bio_2023-2024_assignments) under your own account
        * Here is a [github fork tutorial](https://www.youtube.com/watch?v=a_FLqX3vGR4) 
    * Clone this repository to your local machine
    * Add your username to the line with your student number to the file `students.csv`. **Separate the name and number with a `,`, just like in the example**
    * Commit this change to your repository with an appropriate message
  
    * Consider the directory `handmade_seqs`
        * Add **one** of the following files to this directory:
            * `stnum_handmade_sequences.fasta`
            * `stnum_handmade_sequences_corrected.fasta`
    * Commit this change to your repository with an appropriate message

    * Consider the directory `unique_os_data`
        * Add two files to this directory:
            * `stnum_fstab.txt`
            * `stnum_screenfetch.txt`
    * Commit this change to your repository with an appropriate message

    * Consider the directory `dna_alignments`
        * Add two files to this directory:
            * `stnum_sequences.fasta`
            * `stnum_aligned.fasta`
    * Commit this change to your repository with an appropriate message

    * Details on this task are available [here](https://stuntspt.gitlab.io/prbio_23-24/classes/class_08/index.html#/5) from the linked slide to the end of the presentation

    * Once all this is done, open a "pull request" so that your changes can be included under the main repository


## Epilogue

Knowing how to operate a version control system is an **extremely** important skill for your future, both as a bioinformatician, or even as a software developer. Make sure you take this chance to *really* learn how it works.
